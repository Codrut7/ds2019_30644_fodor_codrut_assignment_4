package ro.codrut.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ro.codrut.soap.UI.MainWindow;

public class SoapApplication {

	public static void main(String[] args) {
		new MainWindow();
	}

}
