package ro.codrut.soap.UI;

import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import ro.codrut.soap.generated.Task;

import java.time.LocalDateTime;

public class Util {

    public static Label createLabel(Shell shell, String text, int style){
        Label label = new Label(shell,style);
        label.setText(text);
        FontData[] fD = label.getFont().getFontData();
        fD[0].setHeight(24);
        label.setFont(new Font(shell.getDisplay(),fD));
        return label;
    }

    public static Button createButton(Shell shell, String text, int style){
        Button button = new Button(shell, style);
        button.setText(text);

        return button;
    }

    public static boolean notifyCaregiver(Task task){
        LocalDateTime tempDateTime = LocalDateTime.from(task.getStartDate().toGregorianCalendar().toZonedDateTime().toLocalDateTime());
        if(task.getActivity().equals("Sleeping")){
            tempDateTime = tempDateTime.plusHours(12);
            return tempDateTime.isBefore(task.getEndDate().toGregorianCalendar().toZonedDateTime().toLocalDateTime());
        }else if(task.getActivity().equals("Leaving")){
            tempDateTime = tempDateTime.plusHours(12);
            return tempDateTime.isBefore(task.getEndDate().toGregorianCalendar().toZonedDateTime().toLocalDateTime());
        }else if(task.getActivity().equals("Toileting")){
            tempDateTime = tempDateTime.plusMinutes(1);
            return tempDateTime.isBefore(task.getEndDate().toGregorianCalendar().toZonedDateTime().toLocalDateTime());
        }
        return false;
    }

}

