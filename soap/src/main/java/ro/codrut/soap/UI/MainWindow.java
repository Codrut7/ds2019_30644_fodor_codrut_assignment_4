package ro.codrut.soap.UI;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ro.codrut.soap.config.Config;
import ro.codrut.soap.config.SOAPConnector;
import ro.codrut.soap.generated.*;

import static org.apache.tomcat.jni.Time.sleep;


public class MainWindow implements CommandLineRunner {

    private Jaxb2Marshaller jax2b;
    private SOAPConnector soapConnector;

    public  MainWindow(){
        initiateGUI();
    }

    public void initiateGUI() {
        Display display = new Display();
        Shell shell = new Shell(display);
        Config config = new Config();
        jax2b = config.marshaller();
        soapConnector = config.soapConnector(jax2b);
        shell.open();
        configureShell(shell);
        shell.setSize(1000,500);
        // run the event loop as long as the window is open
        while (!shell.isDisposed()) {
            // read the next OS event queue and transfer it to a SWT event
            if (!display.readAndDispatch())
            {
                // if there are currently no other OS event to process
                // sleep until the next OS event is available
                display.sleep();
            }
        }

        // disposes all associated windows and their components
        display.dispose();
    }

    private void configureShell(final Shell shell) {
        GridLayout layout = new GridLayout(2, false);
        shell.setLayout(layout);
        Label label = Util.createLabel(shell,"Task history for the patient : ", SWT.NONE);
        StringBuffer buffer = new StringBuffer();
        //get patient history task
        GetPatientHistoryRequest patientHistoryRequest = new GetPatientHistoryRequest();
        patientHistoryRequest.setName("mihaita");
        GetPatientHistoryResponse patientHistoryResponse = (GetPatientHistoryResponse) soapConnector.callWebService("http://localhost:8080/ws/getPatientHistoryRequest", patientHistoryRequest);
        for(Task task : patientHistoryResponse.getTask()){
            buffer.append(task.getActivity() + " from " + task.getStartDate().toString() + " to " + task.getEndDate().toString() + " \n");
            if(Util.notifyCaregiver(task)){
                GetPatientUnsafeTaskRequest taskRequest = new GetPatientUnsafeTaskRequest();
                taskRequest.setActivity(task.getActivity());
                taskRequest.setPatient("mihaita");
                GetPatientUnsafeTaskResponse response = (GetPatientUnsafeTaskResponse) soapConnector.callWebService("http://localhost:8080/ws/getPatientUnsafeTaskRequest", taskRequest);
                System.out.println(response.isNotified());
            }
        }
        Label label2 = Util.createLabel(shell, buffer.toString(), SWT.NONE);
        GetPatientMedicationPlanRequest patientMedicationPlanRequest = new GetPatientMedicationPlanRequest();
        patientMedicationPlanRequest.setName("mihaita");
        System.out.println(soapConnector);
        GetPatientMedicationPlanResponse patientMedicationPlanResponse = (GetPatientMedicationPlanResponse) soapConnector.callWebService("http://localhost:8080/ws/getPatientMedicationPlanRequest", patientMedicationPlanRequest);
        StringBuffer buffer2 = new StringBuffer();
        // get patient medication plan
        for(Medicationplan plan : patientMedicationPlanResponse.getTask()) {
            buffer2.append("Medication plan : " + plan.getId() + " with medications \n");
            for (Medication m : plan.getMedications()) {
                buffer2.append(m.getName() + " ");
                System.out.println(m.getName());
            }
            buffer2.append("\n");
            buffer2.append("With starting date of " + plan.getStartDate().toString() + "\n");
            buffer2.append("With ending date of " + plan.getEndDate().toString() + "\n");
            buffer2.append("Intake is " + plan.getIntake() + "\n");
            if(plan.isTaken()){
                buffer2.append("The medicine is taken. \n");

            }else{
                buffer2.append("The medicine is NOT taken. \n");
            }
        }
        Label label3 = Util.createLabel(shell, buffer2.toString(), SWT.NONE);

    }

    @Override
    public void run(String... args) throws Exception {
        new MainWindow();
    }
}
